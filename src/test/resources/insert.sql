﻿INSERT INTO category (id, name) VALUES (1, 'Footwear');
INSERT INTO category (id, name) VALUES (2, 'Computers');
INSERT INTO category (id, name) VALUES (3, 'Rings');
INSERT INTO category (id, name) VALUES (4, 'Bags');
INSERT INTO category (id, name) VALUES (5, 'Clothes');
INSERT INTO category (id, name) VALUES (6, 'Furniture');


INSERT INTO status (id, name) VALUES (1, 'New');
INSERT INTO status (id, name) VALUES (2, 'Used');


INSERT INTO userclient (id, admin, banned, email, firstname, lastaccess, lastname, negativevotes, neutralvotes, password, positivevotes, secretquestion, secretquestionanswer, username, vip) VALUES (4, false, true, 'lkania@yahoo.com', 'Lucas', '2015-07-14 19:14:46.91', 'Kania', 0, 0, 'asdasd', 0, 'que pregunto?', 'hola', 'lkania', false);
INSERT INTO userclient (id, admin, banned, email, firstname, lastaccess, lastname, negativevotes, neutralvotes, password, positivevotes, secretquestion, secretquestionanswer, username, vip) VALUES (2, true, true, 'ivan.klj@itba.edu.ar', 'Ivan', '2015-07-14 19:43:23.726', 'Kljenak', 0, 0, '1234', 0, 'donde estudias?', 'ITBA', 'ikljenak', false);
INSERT INTO userclient (id, admin, banned, email, firstname, lastaccess, lastname, negativevotes, neutralvotes, password, positivevotes, secretquestion, secretquestionanswer, username, vip) VALUES (3, false, false, 'magopian@hotmail.com', 'Michel', '2015-07-14 19:56:08.189', 'Agopian', 0, 2, 'hola', 0, 'nombre de mi mascota?', 'Pepito', 'mishuagopian', true);
INSERT INTO userclient (id, admin, banned, email, firstname, lastaccess, lastname, negativevotes, neutralvotes, password, positivevotes, secretquestion, secretquestionanswer, username, vip) VALUES (5, false, false, 'jpe@paw.com', 'Joaquin', '2015-07-14 19:54:43.279', 'Perez', 0, 1, 'pass', 1, 'quien soy?', 'jperez', 'jp', true);
INSERT INTO userclient (id, admin, banned, email, firstname, lastaccess, lastname, negativevotes, neutralvotes, password, positivevotes, secretquestion, secretquestionanswer, username, vip) VALUES (1, true, false, 'nico.dinu@gmail.com', 'Nicolas', '2015-07-14 19:58:01.097', 'Di Nucci', 1, 1, '123123', 2, 'como te llamas?', 'Nicolas', 'ndinu', false);


INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (6, 'Dasani 600ml', NULL, NULL, NULL, false, 'Bottled Water', 1.5, '2015-07-14 19:24:14.529', 0, 250, false, 11, 2, 1);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (8, 'Great quality, highly recommended.', NULL, NULL, NULL, true, 'Glass', 30, '2015-07-14 19:28:39.973', 58, 0, false, 9, 3, 2);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (9, 'really sweet', NULL, NULL, NULL, true, 'Lollypop', 12, '2015-07-14 19:55:57.263', 13, 907, false, 4, 5, 1);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (3, 'Classic', NULL, NULL, NULL, false, 'Nike t-shirts', 500, '2015-07-14 19:21:43.554', 7, 26, false, 8, 1, 1);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (4, '13inch 2013 model', NULL, NULL, NULL, false, 'MacBook Pro', 400, '2015-07-14 19:22:26.463', 0, 2, true, 3, 1, 2);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (7, '600W', NULL, NULL, NULL, false, 'Sony Audio System', 896, '2015-07-14 19:26:54.342', 0, 54, true, 1, 3, 2);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (1, '19 carat gold', NULL, NULL, NULL, false, 'Golden Ring', 5557, '2015-07-14 19:20:04.183', 2, 1, false, 4, 1, 1);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (5, '2x5 meters ward wood', NULL, NULL, NULL, false, 'Wooden Table', 4008, '2015-07-14 19:23:26.868', 0, 5, false, 5, 2, 1);
INSERT INTO article (id, description, format, img, imgname, isprioritized, name, price, publicationdate, purchases, stock, suspended, visits, owner_id, status_id) VALUES (2, 'For all men and women.', NULL, NULL, NULL, true, 'XL Bag', 133.300003, '2015-07-14 19:20:56.048', 1, 48, false, 4, 1, 2);



INSERT INTO article_category (articles_id, categories_id) VALUES (1, 3);
INSERT INTO article_category (articles_id, categories_id) VALUES (2, 4);
INSERT INTO article_category (articles_id, categories_id) VALUES (2, 5);
INSERT INTO article_category (articles_id, categories_id) VALUES (3, 5);
INSERT INTO article_category (articles_id, categories_id) VALUES (3, 6);
INSERT INTO article_category (articles_id, categories_id) VALUES (4, 4);
INSERT INTO article_category (articles_id, categories_id) VALUES (4, 6);
INSERT INTO article_category (articles_id, categories_id) VALUES (4, 2);
INSERT INTO article_category (articles_id, categories_id) VALUES (5, 4);
INSERT INTO article_category (articles_id, categories_id) VALUES (5, 1);
INSERT INTO article_category (articles_id, categories_id) VALUES (5, 6);
INSERT INTO article_category (articles_id, categories_id) VALUES (6, 1);
INSERT INTO article_category (articles_id, categories_id) VALUES (6, 5);
INSERT INTO article_category (articles_id, categories_id) VALUES (7, 1);
INSERT INTO article_category (articles_id, categories_id) VALUES (7, 5);
INSERT INTO article_category (articles_id, categories_id) VALUES (7, 2);
INSERT INTO article_category (articles_id, categories_id) VALUES (8, 4);
INSERT INTO article_category (articles_id, categories_id) VALUES (8, 6);
INSERT INTO article_category (articles_id, categories_id) VALUES (9, 1);
INSERT INTO article_category (articles_id, categories_id) VALUES (9, 5);
INSERT INTO article_category (articles_id, categories_id) VALUES (9, 6);


INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (1, 1, 'Hope its good!', false, false, '2015-07-14 19:34:52.411', 2, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (2, 2, 'Loved those', true, false, '2015-07-14 19:37:50.194', 3, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (3, 10, 'I loved it, same place same time.', false, true, '2015-07-14 19:38:38.314', 3, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (5, 3, 'I need it now!', true, false, '2015-07-14 19:40:20.932', 8, 5);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (6, 2, 'i am getting married!', true, false, '2015-07-14 19:40:46.01', 1, 5);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (7, 55, 'I want them all!', true, false, '2015-07-14 19:45:58.524', 8, 5);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (8, 1, 'great bag', true, false, '2015-07-14 19:49:00.671', 2, 5);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (9, 2, 'dryfit?', true, false, '2015-07-14 19:52:40.783', 3, 5);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (12, 30, 'these are adictive', false, false, '2015-07-14 19:57:00.122', 9, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (13, 50, 'bring more please', false, false, '2015-07-14 19:57:10.845', 9, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (4, 3, 'We can met in the mall to make the transaction.', true, false, '2015-07-14 19:39:22.891', 3, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (11, 12, 'i want more of those!', true, false, '2015-07-14 19:56:40.767', 9, 3);
INSERT INTO purchase (id, amount, comment, isconfirmed, isreported, purchasedate, article_id, buyer_id) VALUES (10, 1, 'give me 1', true, false, '2015-07-14 19:56:17.136', 9, 3);


INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (1, NULL, '2015-07-14 19:32:22.683', 'Can you bring it to Buenos Aires?', 5, 1);
INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (2, NULL, '2015-07-14 19:33:03.216', 'Could i get any discount?', 5, 1);
INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (3, NULL, '2015-07-14 19:36:59.908', 'Can i buy it today?', 6, 3);
INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (4, NULL, '2015-07-14 19:37:08.562', 'Is it pure?', 6, 3);
INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (6, NULL, '2015-07-14 19:43:40.081', 'Any units coming any soon?', 4, 2);
INSERT INTO query (id, answer, date, query, article_id, creator_id) VALUES (5, 'Sorry, no more till september.', '2015-07-14 19:42:17.907', 'Are the any more units? i m very interested', 6, 5);


INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (3, 'Dont trust this person ever!');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (3, 'All were broken');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (1, 'Nice guy');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (1, 'He was late to the trade');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (1, 'didnt like it');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (1, 'He ll scam you');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (5, 'good people');
INSERT INTO userclient_commentsfromusers (userclient_id, comment) VALUES (5, 'one again , in time and fast');


ALTER SEQUENCE article_id_seq
  RESTART 10;
ALTER SEQUENCE category_id_seq
  RESTART 7;
 ALTER SEQUENCE purchase_id_seq
  RESTART 14;
ALTER SEQUENCE query_id_seq
  RESTART 7;
ALTER SEQUENCE status_id_seq
  RESTART 3;
ALTER SEQUENCE userclient_id_seq
  RESTART 6;