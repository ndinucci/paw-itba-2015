package web.pages.secured;

import web.PawShopSession;
import web.pages.base.BasePage;
import web.pages.signIn.SignInPage;

@SuppressWarnings("serial")
public abstract class SecuredPage extends BasePage {

	public SecuredPage() {
		PawShopSession session = PawShopSession.get();
		if (!session.isSignedIn()) {
			redirectToInterceptPage(new SignInPage());	
		}
	}
}