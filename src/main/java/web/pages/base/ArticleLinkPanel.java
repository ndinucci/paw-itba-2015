package web.pages.base;

import models.Article;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import web.pages.article.ArticlePage;

@SuppressWarnings("serial")
public class ArticleLinkPanel extends Panel {

	
	private IModel<Article> articleModel;
	
	public ArticleLinkPanel(String id, IModel<Article> model) {
		super(id);
		initialize(model);
		articleModel = model;
	}

	private void initialize(IModel<Article> model) {
		Link<ArticlePage> link = new BookmarkablePageLink<ArticlePage>("name", ArticlePage.class, 
				new PageParameters().add("articleId", model.getObject().getId()));
		link.add(new Label("span",  new PropertyModel<String>(model, "name")));
		add(link);
		add(new Label("suspended", new StringResourceModel("suspended", this, null)){
			@Override
			protected void onBeforeRender() {
				setVisible(articleModel.getObject().getSuspended());
				super.onBeforeRender();
			}
		});
	}
	
}
