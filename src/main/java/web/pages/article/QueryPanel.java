package web.pages.article;

import java.util.Date;

import models.EntityModel;
import models.EntityResolver;
import models.Query;
import models.QueryRepo;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;
import web.pages.base.DatePanel;

@SuppressWarnings("serial")
public class QueryPanel extends Panel {

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private QueryRepo queryRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public QueryPanel(String id, IModel<Query> model) {
		super(id);
		initialize(model);
	}

	private void initialize(IModel<Query> model) {
		add(new Label("creatorUsername", new PropertyModel<String>(new EntityModel<UserClient>(UserClient.class, model.getObject().getCreator()), "username")));
		add(new DatePanel("queryDate", new PropertyModel<Date>(model, "date")));
		add(new Label("query", new PropertyModel<String>(model, "query")));
		add(new Label("answer", new PropertyModel<String>(model, "answer")));
		
		add(new AnswerPanel("answerPanel", model));
		
		Link<Query> link = new Link<Query>("delete", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.deleteQuery(getModelObject());
				queryRepoInstance.delete(getModelObject());
				setResponsePage(ArticlePage.class, new PageParameters().add("articleId", getModelObject().getArticle().getId()));
			}
			
			@Override
			protected void onBeforeRender() {
				PawShopSession s = PawShopSession.get();
				setVisible(s.isSignedIn() && getModelObject().getArticle().getOwner().getUsername().equals(s.getUsername()));
				super.onBeforeRender();
			}
		};
		add(link);
	}
	
}
