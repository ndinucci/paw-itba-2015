package web.pages.article;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.ArticleRepo;
import models.EntityModel;
import models.EntityResolver;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.ArticleLinkPanel;

@SuppressWarnings("serial")
public class SuggestionsPanel extends Panel {
	
	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	public SuggestionsPanel(String id, IModel<Article> model) {
		super(id);
		initialize(model);
	}

	private void initialize(IModel<Article> model) {
		add(new RefreshingView<Article>("article", new EntityModel<Article>(Article.class, model.getObject())) {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Article>> getItemModels() {
				List<IModel<Article>> result = new ArrayList<IModel<Article>>();
				for (Article a :articleRepoInstance.getSuggestions(3, ((Article)getDefaultModelObject()))) {
					final int id = a.getId(); 
					result.add(new LoadableDetachableModel<Article>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Article load() {
							return entityResolver.fetch(Article.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Article> item) {
				item.add(new ArticleLinkPanel("articleLink", item.getModel()));
			}
		});
	}

}
