package web.pages.article;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.EntityModel;
import models.EntityResolver;
import models.Query;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ArticleQueriesPanel extends Panel {
	
	@SpringBean
	private EntityResolver entityResolver;
	
	public ArticleQueriesPanel(String id, IModel<Article> model) {
		super(id);
		initialize(model);
	}

	private void initialize(IModel<Article> model) {
		add(new RefreshingView<Query>("queries", new EntityModel<Article>(Article.class, model.getObject())) {
			
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Query>> getItemModels() {
				List<IModel<Query>> result = new ArrayList<IModel<Query>>();
				for (Query q :((Article)getDefaultModelObject()).getQueries()) {
					final int id = q.getId(); 
					result.add(new LoadableDetachableModel<Query>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Query load() {
							return entityResolver.fetch(Query.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Query> item) {
				item.add(new QueryPanel("query", item.getModel()));
			}
		});
	}
}
