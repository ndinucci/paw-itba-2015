package web.pages.article;

import java.io.IOException;
import java.util.Date;

import models.Article;
import models.ArticleRepo;
import models.EntityModel;
import models.Status;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;

import web.PawShopSession;
import web.pages.base.BasePage;
import web.pages.base.DatePanel;
import web.pages.buy.BuyPage;
import web.pages.home.HomePage;
import web.pages.userPublicProfile.UserPublicProfilePage;
import web.pages.userPublicProfile.VipPanel;

@SuppressWarnings("serial")
public class ArticlePage extends BasePage {

	@SpringBean
	private ArticleRepo articleRepoInstance;

	@SpringBean
	private UserRepo userRepoInstance;

	private EntityModel<Article> articleModel;

	private int id;

	public ArticlePage(PageParameters params) {
		StringValue articleId = params.get("articleId");
		if (articleId == null) {
			throw new RuntimeException();
		}
		Article article = articleRepoInstance.getById(articleId.toInt());
		if (article == null) {
			throw new RuntimeException();
		}
		id = article.getId();

		articleModel = new EntityModel<Article>(Article.class, article);

		add(new VipPanel("vipUser", new PropertyModel<UserClient>(
				articleModel, "owner")));

		add(new Label("name", new PropertyModel<String>(articleModel, "name")));

		IResource imageResource = new DynamicImageResource() {
			@Override
			protected byte[] getImageData(IResource.Attributes attributes) {
				try {
					return articleModel.getObject().getImageBytes();
				} catch (IOException e) {
					return null;
				}
			}
		};

		add(new Image("articleImage", imageResource));

		add(new Link<Article>("buyButton", new EntityModel<Article>(
				Article.class, article)) {
			@Override
			public void onClick() {
				setResponsePage(new BuyPage(getModel()));
			}

			@Override
			protected void onBeforeRender() {
				PawShopSession s = PawShopSession.get();
				setVisible(s.isSignedIn()
						&& getModelObject().getStock() != 0
						&& !s.getUsername().equals(
								getModelObject().getOwner().getUsername())
						&& !userRepoInstance.getByUsername(s.getUsername())
								.getBanned()
						&& !getModelObject().getOwner().getBanned()
						&& !getModelObject().getSuspended());
				super.onBeforeRender();
			};
		});

		add(new Label("soldOut", new StringResourceModel("soldOut", this, null)) {
			@Override
			protected void onBeforeRender() {
				setVisible(((Article) articleModel.getObject()).getStock() == 0);
				super.onBeforeRender();
			}
		});

		add(new ArticleCategoriesPanel("articleCategories",
				new EntityModel<Article>(Article.class, article)));

		add(new Label("status", new PropertyModel<String>(
				new EntityModel<Status>(Status.class, articleModel.getObject()
						.getStatus()), "name")));
		add(new Label("description", new PropertyModel<String>(articleModel,
				"description")));
		add(new Label("price", new PropertyModel<Float>(articleModel, "price")));
		add(new Label("stock",
				new PropertyModel<Integer>(articleModel, "stock")));
		add(new Label("visits", new PropertyModel<Integer>(articleModel,
				"visits")));
		add(new DatePanel("publicationDate", new PropertyModel<Date>(
				articleModel, "publicationDate")));

		Link<UserPublicProfilePage> link = new BookmarkablePageLink<UserPublicProfilePage>(
				"usernameLink", UserPublicProfilePage.class,
				new PageParameters().add("username", articleModel.getObject()
						.getOwner().getUsername()));
		link.add(new Label("username", new PropertyModel<String>(
				new EntityModel<UserClient>(UserClient.class, articleModel
						.getObject().getOwner()), "username")));
		add(link);

		add(new Label("positiveVotes", new PropertyModel<Integer>(
				new EntityModel<UserClient>(UserClient.class, articleModel
						.getObject().getOwner()), "positiveVotes")));
		add(new Label("negativeVotes", new PropertyModel<Integer>(
				new EntityModel<UserClient>(UserClient.class, articleModel
						.getObject().getOwner()), "negativeVotes")));
		add(new Label("neutralVotes", new PropertyModel<Integer>(
				new EntityModel<UserClient>(UserClient.class, articleModel
						.getObject().getOwner()), "neutralVotes")));

		add(new SameOwnerPanel("sameOwner", new EntityModel<Article>(
				Article.class, article)));
		add(new SuggestionsPanel("suggestions", new EntityModel<Article>(
				Article.class, article)));

		add(new ArticleQueriesPanel("queries", new EntityModel<Article>(
				Article.class, article)));
		add(new AskPanel("ask",
				new EntityModel<Article>(Article.class, article)));
	}

	@Override
	protected void onBeforeRender() {
		Article article = articleRepoInstance.getById(id);
		if (article == null) {
			setResponsePage(HomePage.class);
		}
		article.visit();
		super.onBeforeRender();
	}

}
