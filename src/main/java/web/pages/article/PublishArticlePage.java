package web.pages.article;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Article;
import models.ArticleRepo;
import models.Category;
import models.CategoryRepo;
import models.Status;
import models.StatusRepo;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;
import web.pages.home.HomePage;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class PublishArticlePage extends SecuredPage {

	@SpringBean
	private ArticleRepo articleRepoInstance;

	@SpringBean
	private CategoryRepo categoryRepoInstance;

	@SpringBean
	private StatusRepo statusRepoInstance;

	@SpringBean
	private UserRepo userRepoInstance;

	private transient String name;
	private transient String description;
	private transient ArrayList<String> selectedCategory = new ArrayList<String>();
	private transient String selectedStatus;
	private transient Float price;
	private transient Integer stock;
	private transient Boolean isPrioritized;
	private transient List<FileUpload> fileUpload;

	public PublishArticlePage() {

		add(new FeedbackPanel("feedback"));
		Form<PublishArticlePage> form = new Form<PublishArticlePage>(
				"publishArticleForm",
				new CompoundPropertyModel<PublishArticlePage>(this)) {
			@Override
			protected void onSubmit() {
				PawShopSession session = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(session
						.getUsername());
				Set<Category> cats = new HashSet<Category>();
				for (String s : selectedCategory) {
					cats.add(categoryRepoInstance.getByName(s));
				}
				
				byte[] imgBytes = null;
				String imgName = null;
				String imgType = null;
				if( fileUpload != null && fileUpload.size()>0){
					FileUpload uploadedFile = fileUpload.get(0);
					imgBytes = uploadedFile.getBytes();
					imgName = uploadedFile.getClientFileName();
					imgType = uploadedFile.getContentType();
				}
				
				articleRepoInstance.publish(new Article(stock, price, name, description,
						new Date(), cats,
						statusRepoInstance.getByName(selectedStatus), user,
						isPrioritized, imgBytes, imgName, imgType));
				setResponsePage(HomePage.class);
			}
		};
		form.add(new TextField<String>("name").add(
				new MaximumLengthValidator(Article.NAME_MAX_SIZE)).setRequired(
				true));
		form.add(new TextField<String>("description").add(
				new MaximumLengthValidator(Article.DESCRIPTION_MAX_SIZE))
				.setRequired(true));

		Set<Category> categories = categoryRepoInstance.getAll();
		List<String> categoriesList = new ArrayList<String>();
		for (Category c : categories) {
			categoriesList.add(c.getName());
		}
		ListMultipleChoice<String> categoriesDropdDown = new ListMultipleChoice<String>(
				"categories", new Model<ArrayList<String>>(selectedCategory),
				categoriesList) {
		};
		form.add(categoriesDropdDown.setRequired(true));

		form.add(new TextField<Float>("price").add(
				new RangeValidator<Float>(0.0f, Float.MAX_VALUE)).setRequired(
				true));
		form.add(new TextField<Integer>("stock").add(
				new RangeValidator<Integer>(0, Integer.MAX_VALUE)).setRequired(
				true));

		Set<Status> status = statusRepoInstance.getAll();
		List<String> statusList = new ArrayList<String>();
		for (Status s : status) {
			statusList.add(s.getName());
		}
		DropDownChoice<String> statusDropdDown = new DropDownChoice<String>(
				"status", new PropertyModel<String>(this, "selectedStatus"),
				statusList);
		form.add(statusDropdDown.setRequired(true));

		form.add(new CheckBox("isPrioritized"));
		form.add(new FileUploadField("fileUpload"));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
}
