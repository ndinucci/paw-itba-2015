package web.pages.article;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import models.Article;
import models.ArticleRepo;
import models.Category;
import models.CategoryRepo;
import models.EntityModel;
import models.Status;
import models.StatusRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class EditArticlePage extends SecuredPage {
	
	private int id;
	
	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private CategoryRepo categoryRepoInstance;
	
	@SpringBean
	private StatusRepo statusRepoInstance;
	
	private transient String name;
	private transient String description;
	private transient ArrayList<String> selectedCategory = new ArrayList<String>();
	private transient String selectedStatus;
	private transient Boolean isPrioritized;
	private transient List<FileUpload> fileUpload;
	
	public EditArticlePage(PageParameters params){
		StringValue articleId = params.get("articleId");
		if (articleId == null) {
			throw new RuntimeException();
		}
		Article article = articleRepoInstance.getById(articleId.toInt());
		if (article == null) {
			throw new RuntimeException();
		}
		id = article.getId();
		setDefaultModel(new CompoundPropertyModel<Article>(
				new EntityModel<Article>(Article.class, article)));
		
		EntityModel<Article> articleModel = new EntityModel<Article>(Article.class, article);
		
		PropertyModel<Status> statusModel = new PropertyModel<Status>(articleModel, "status");
		selectedStatus = statusModel.getObject().getName();
		name = articleModel.getObject().getName();
		description = articleModel.getObject().getDescription();
		PropertyModel<Set<Category>> categoriesModel = new PropertyModel<Set<Category>>(articleModel, "categories");
		for(Category c: categoriesModel.getObject()){
			selectedCategory.add(c.getName());
		}
		
		add(new FeedbackPanel("feedback"));
		Form<EditArticlePage> form = new Form<EditArticlePage>("editArticleForm", new CompoundPropertyModel<EditArticlePage>(this)) {
			@Override
			protected void onSubmit() {
				article().setName(name);
				article().setDescription(description);
				article().setStatus(statusRepoInstance.getByName(selectedStatus));
				Set<Category> cats = new HashSet<Category>();
				for(String s: selectedCategory){
					cats.add(categoryRepoInstance.getByName(s));
				}
				article().setCategories(cats);
				article().setPrioritized(isPrioritized);
				
				byte[] imgBytes = null;
				String imgName = null;
				String imgType = null;
				if( fileUpload != null && fileUpload.size()>0){
					FileUpload uploadedFile = fileUpload.get(0);
					imgBytes = uploadedFile.getBytes();
					imgName = uploadedFile.getClientFileName();
					imgType = uploadedFile.getContentType();
				}
				article().setImage(imgBytes, imgName, imgType);
				setResponsePage(ArticlePage.class, new PageParameters().add("articleId", id));
			}
		};
		form.add(new TextField<String>("name").add(new MaximumLengthValidator(Article.NAME_MAX_SIZE)));
		form.add(new TextField<String>("description").add( new MaximumLengthValidator(Article.DESCRIPTION_MAX_SIZE)));

		Set<Category> categories = categoryRepoInstance.getAll();
		List<String> categoriesList = new ArrayList<String>();
		for (Category c : categories) {
			categoriesList.add(c.getName());
		}
		ListMultipleChoice<String> categoriesDropdDown = new ListMultipleChoice<String>(
				"categories", new Model<ArrayList<String>>(selectedCategory), categoriesList){
		};
		form.add(categoriesDropdDown);
		
		Set<Status> status = statusRepoInstance.getAll();
		List<String> statusList = new ArrayList<String>();
		for (Status s : status) {
			statusList.add(s.getName());
		}
		DropDownChoice<String> statusDropdDown = new DropDownChoice<String>(
				"status", new PropertyModel<String>(this, "selectedStatus"), statusList);
		form.add(statusDropdDown);
		
		form.add(new CheckBox("isPrioritized"));
		form.add(new FileUploadField("fileUpload"));
		
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
	private Article article() {
		return (Article) getDefaultModelObject();
	}
}
