package web.pages.userPrivateProfile;

import models.EntityModel;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class ChangeUserPasswordPage extends SecuredPage {
	@SpringBean
	private UserRepo userRepoInstance;

	private transient String oldPassword;
	private transient String newPassword;
	private transient String newPasswordRepeat;

	public ChangeUserPasswordPage() {
		PawShopSession session = PawShopSession.get();
		String username = session.getUsername();
		UserClient user = userRepoInstance.getByUsername(username);

		setDefaultModel(new CompoundPropertyModel<UserClient>(
				new EntityModel<UserClient>(UserClient.class, user)));

		add(new FeedbackPanel("feedback"));
		Form<ChangeUserPasswordPage> form = new Form<ChangeUserPasswordPage>(
				"newPasswordForm",
				new CompoundPropertyModel<ChangeUserPasswordPage>(this)) {
			@Override
			protected void onSubmit() {
				if (userClient().checkPassword(oldPassword)) {
					if (newPassword.equals(newPasswordRepeat)) {
						userClient().setPassword(newPassword);
						setResponsePage(UserPrivateProfilePage.class);
					} else {
						error("passwords must coincide");
					}
				}
				else {
					error("incorrect old password");
				}
			}
		};
		form.add(new PasswordTextField("oldPassword").add(
				new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE))
				.setRequired(true));
		form.add(new PasswordTextField("newPassword").add(
				new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE))
				.setRequired(true));
		form.add(new PasswordTextField("newPasswordRepeat").add(
				new MaximumLengthValidator(UserClient.PASSWORD_MAX_SIZE))
				.setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}

	private UserClient userClient() {
		return (UserClient) getDefaultModelObject();
	}
}
