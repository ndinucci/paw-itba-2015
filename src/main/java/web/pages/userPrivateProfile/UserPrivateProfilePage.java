package web.pages.userPrivateProfile;

import org.apache.wicket.markup.html.link.Link;

import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class UserPrivateProfilePage extends SecuredPage {
	public UserPrivateProfilePage() {
		add(new Link<Void>("purchases") {
			@Override
			public void onClick() {
				setResponsePage(PurchasesPage.class);
			}
		});
		
		add(new Link<Void>("sales") {
			@Override
			public void onClick() {
				setResponsePage(SalesPage.class);
			}
		});
		
		add(new Link<Void>("articles") {
			@Override
			public void onClick() {
				setResponsePage(ProfileArticlePage.class);
			}
		});
		
		add(new Link<Void>("pending") {
			@Override
			public void onClick() {
				setResponsePage(PendingQueriesPage.class);
			}
		});
		
		add(new Link<Void>("changeInfo") {
			@Override
			public void onClick() {
				setResponsePage(ChangeInfoPage.class);
			}
		});
		
		add(new Link<Void>("changePassword") {
			@Override
			public void onClick() {
				setResponsePage(ChangeUserPasswordPage.class);
			}
		});
	}
}
