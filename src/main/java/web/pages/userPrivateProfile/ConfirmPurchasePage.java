package web.pages.userPrivateProfile;

import java.util.ArrayList;
import java.util.List;

import models.Comment;
import models.Purchase;
import models.UserClient;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class ConfirmPurchasePage extends SecuredPage {

	
	private transient String comment;
	private transient String vote;
	private IModel<Purchase> purchaseModel;
	
	public ConfirmPurchasePage(IModel<Purchase> model) {
		
		add(new FeedbackPanel("feedback"));
		purchaseModel = model;
		Form<ConfirmPurchasePage> form = new Form<ConfirmPurchasePage>("confirmForm", new CompoundPropertyModel<ConfirmPurchasePage>(this)) {
			@Override
			protected void onSubmit() {
				if(comment != null && !comment.equals("")){
					purchaseModel.getObject().getArticle().getOwner().addComment(new Comment(comment));
				}
				if(vote != null){	
					if(vote.equals(new StringResourceModel("negative", this, null).getString())){
						purchaseModel.getObject().getArticle().getOwner().voteNegative();
					}
					if(vote.equals(new StringResourceModel("neutral", this, null).getString())){
						purchaseModel.getObject().getArticle().getOwner().voteNeutral();
					}
					if(vote.equals(new StringResourceModel("positive", this, null).getString())){
						purchaseModel.getObject().getArticle().getOwner().votePositive();
					}
				}
				setResponsePage(PurchasesPage.class);
			}
		};
		form.add(new TextField<String>("comment").add(new MaximumLengthValidator(UserClient.COMMENT_MAX_SIZE)));
		List<String> voteList = new ArrayList<String>();
		voteList.add(new StringResourceModel("positive", this, null).getString());
		voteList.add(new StringResourceModel("negative", this, null).getString());
		voteList.add(new StringResourceModel("neutral", this, null).getString());
		DropDownChoice<String> voteDropDown = new DropDownChoice<String>(
				"vote", new PropertyModel<String>(this, "vote"), voteList);
		form.add(voteDropDown);
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
}
