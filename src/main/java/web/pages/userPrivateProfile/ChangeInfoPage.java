package web.pages.userPrivateProfile;

import models.EntityModel;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;
import web.pages.secured.SecuredPage;

@SuppressWarnings("serial")
public class ChangeInfoPage extends SecuredPage {
	@SpringBean
	private UserRepo userRepoInstance;

	private transient String firstname;
	private transient String lastname;
	private transient String email;
	private transient String username;

	public ChangeInfoPage() {
		PawShopSession session = PawShopSession.get();
		String username = session.getUsername();
		UserClient user = userRepoInstance.getByUsername(username);

		setDefaultModel(new CompoundPropertyModel<UserClient>(
				new EntityModel<UserClient>(UserClient.class, user)));
		firstname = userClient().getFirstName();
		lastname = userClient().getLastName();
		email = userClient().getEmail();
		this.username = userClient().getUsername();
		
		add(new FeedbackPanel("feedback"));
		Form<ChangeInfoPage> form = new Form<ChangeInfoPage>("changeInfoForm",
				new CompoundPropertyModel<ChangeInfoPage>(this)) {
			@Override
			protected void onSubmit() {
				UserClient byEmail = userRepoInstance.getByEmail(email);
				UserClient byUsername = userRepoInstance
						.getByUsername(ChangeInfoPage.this.username);
				if (byEmail != null && !byEmail.equals(userClient())) {
					error("email aready in use");
				} else if (byUsername != null && !byUsername.equals(userClient())) {
					error("username aready in use");
				} else {
					userClient().setEmail(email);
					userClient().setUsername(ChangeInfoPage.this.username);
					userClient().setFirstName(firstname);
					userClient().setLastName(lastname);
					setResponsePage(UserPrivateProfilePage.class);
				}
			}
		};
		form.add(new TextField<String>("lastname").add(
				new MaximumLengthValidator(UserClient.LASTNAME_MAX_SIZE))
				.setRequired(true));
		form.add(new TextField<String>("firstname").add(
				new MaximumLengthValidator(UserClient.FIRSTNAME_MAX_SIZE))
				.setRequired(true));
		form.add(new TextField<String>("email")
				.add(new MaximumLengthValidator(UserClient.EMAIL_MAX_SIZE))
				.setRequired(true).add(EmailAddressValidator.getInstance()));
		form.add(new TextField<String>("username").add(
				new MaximumLengthValidator(UserClient.USERNAME_MAX_SIZE))
				.setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}

	private UserClient userClient() {
		return (UserClient) getDefaultModelObject();
	}
}
