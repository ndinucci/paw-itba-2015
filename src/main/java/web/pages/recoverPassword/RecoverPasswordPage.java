package web.pages.recoverPassword;

import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.base.BasePage;

@SuppressWarnings("serial")
public class RecoverPasswordPage extends BasePage {

	@SpringBean
	private UserRepo userRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;
	
	private transient String username;
	
	public RecoverPasswordPage() {
		add(new FeedbackPanel("feedback"));
		Form<RecoverPasswordPage> form = new Form<RecoverPasswordPage>("recoverPasswordForm", new CompoundPropertyModel<RecoverPasswordPage>(this)) {
			@Override
			protected void onSubmit() {
				UserClient user = userRepoInstance.getByUsername(username);
				if (user != null) {
					setResponsePage(SecretAnswerPage.class, new PageParameters().add("username", user.getUsername()));
				} else {
					error(new StringResourceModel("invalidUsername", this, null).getString());
				}
			}
		};
		form.add(new TextField<String>("username").add(new MaximumLengthValidator(UserClient.USERNAME_MAX_SIZE)).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
}