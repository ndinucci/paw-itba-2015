package web.pages.buy;


import models.Article;
import models.EntityModel;
import models.Purchase;
import models.Status;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;
import org.apache.wicket.validation.validator.MinimumValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.PawShopSession;
import web.pages.article.ArticleCategoriesPanel;
import web.pages.base.BasePage;

@SuppressWarnings("serial")
public class BuyPage extends BasePage {

	@SpringBean
	private UserRepo userRepoInstance;
	
	private transient Integer amount;
	private transient String comment;
	private IModel<Article> articleModel;
	
	public BuyPage(IModel<Article> model) {
		add(new FeedbackPanel("feedback"));
		articleModel = model;
		add(new Label("name",new PropertyModel<String>(articleModel , "name")));
		add(new ArticleCategoriesPanel("articleCategories", articleModel));
		add(new Label("status",new PropertyModel<String>(new EntityModel<Status>(Status.class, articleModel.getObject().getStatus()) , "name")));
		add(new Label("description",new PropertyModel<String>(articleModel , "description")));
		add(new Label("price",new PropertyModel<Double>(articleModel , "price")));
		add(new Label("stock",new PropertyModel<Integer>(articleModel , "stock")));
		Form<BuyPage> form = new Form<BuyPage>("buyForm", new CompoundPropertyModel<BuyPage>(this)) {
			@Override
			protected void onSubmit() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.purchase(articleModel.getObject(), amount, comment);
				setResponsePage(new OwnerInfoPage(articleModel));
			}
		};
		form.add(new TextField<String>("comment").add(new MaximumLengthValidator(Purchase.COMMENT_MAX_SIZE)).setRequired(true));
		IValidator<Integer> stockValidator = new IValidator<Integer>(){
			public void validate(IValidatable<Integer> validatable) {
				final Integer formAmount = validatable.getValue();
				if(formAmount > articleModel.getObject().getStock()){
					validatable.error(new ValidationError().addMessageKey("noStock"));
				}
			}
		};
		form.add(new TextField<Integer>("amount").add(new MinimumValidator<Integer>(1)).add(stockValidator).setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
	}
	
}
