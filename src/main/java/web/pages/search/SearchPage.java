package web.pages.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import models.Article;
import models.Category;
import models.CategoryRepo;
import models.QueryObject;
import models.Status;
import models.StatusRepo;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.RangeValidator;
import org.apache.wicket.validation.validator.StringValidator.MaximumLengthValidator;

import web.pages.base.BasePage;

@SuppressWarnings("serial")
public class SearchPage extends BasePage {
	
	@SpringBean
	private CategoryRepo categoryRepoInstance;
	
	@SpringBean
	private StatusRepo statusRepoInstance;

	private transient String name;
	private transient Float from;
	private transient Float to;
	private transient String selectedCategory = new StringResourceModel("all", this, null).getString();
	private transient String selectedStatus = new StringResourceModel("all", this, null).getString();
	private transient String selectedOrderField = new StringResourceModel("name", this, null).getString();
	private transient String selectedOrderOrder = new StringResourceModel("ascending", this, null).getString();
	
	public SearchPage(QueryObject queryObject) {
		setSearchValuesInForm(queryObject);
		add(new FeedbackPanel("feedback"));
		Form<SearchPage> form = new Form<SearchPage>("searchForm", new CompoundPropertyModel<SearchPage>(this)) {
			@Override
			protected void onSubmit() {
				QueryObject queryObject = submitQueryObject();
				setResponsePage(new SearchPage(queryObject));
			}
		};
		form.add(new TextField<String>("name").add(new MaximumLengthValidator(Article.NAME_MAX_SIZE)));
		form.add(new TextField<Float>("from").add(new RangeValidator<Float>(Article.MIN_PRICE, Float.MAX_VALUE)));
		form.add(new TextField<Float>("to").add(new RangeValidator<Float>(Article.MIN_PRICE, Float.MAX_VALUE)));

		Set<Category> categories = categoryRepoInstance.getAll();
		List<String> categoriesList = new ArrayList<String>();
		categoriesList.add(new StringResourceModel("all", this, null).getString());
		for (Category c : categories) {
			categoriesList.add(c.getName());
		}
		DropDownChoice<String> categoriesDropDown = new DropDownChoice<String>(
				"category", new PropertyModel<String>(this, "selectedCategory"), categoriesList);
		form.add(categoriesDropDown);
		
		Set<Status> status = statusRepoInstance.getAll();
		List<String> statusList = new ArrayList<String>();
		statusList.add(new StringResourceModel("all", this, null).getString());
		for (Status s : status) {
			statusList.add(s.getName());
		}
		DropDownChoice<String> statusDropDown = new DropDownChoice<String>(
				"status", new PropertyModel<String>(this, "selectedStatus"), statusList);
		form.add(statusDropDown);
		
		List<String> orderFieldList = new ArrayList<String>();
		orderFieldList.add(new StringResourceModel("name", this, null).getString());
		orderFieldList.add(new StringResourceModel("price", this, null).getString());
		orderFieldList.add(new StringResourceModel("date", this, null).getString());
		DropDownChoice<String> orderFieldDropDown = new DropDownChoice<String>(
				"orderField", new PropertyModel<String>(this, "selectedOrderField"), orderFieldList);
		form.add(orderFieldDropDown);
		
		List<String> orderOrderList = new ArrayList<String>();
		orderOrderList.add(new StringResourceModel("ascending", this, null).getString());
		orderOrderList.add(new StringResourceModel("descending", this, null).getString());
		DropDownChoice<String> orderOrderDropDown = new DropDownChoice<String>(
				"orderOrder", new PropertyModel<String>(this, "selectedOrderOrder"), orderOrderList);
		form.add(orderOrderDropDown);
		
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
		
		add(new ArticleSearchPanel("articleSearchPanel", queryObject));
	}

	private QueryObject submitQueryObject() {
		QueryObject qo = new QueryObject();		
		if (name != null && !name.equals("")) {
			qo.setName(name);
		}
		if (from != null) {
			qo.setMinPrice(Float.valueOf(from));
		}
		if (to != null) {
			qo.setMaxPrice(Float.valueOf(to));
		}
		if (selectedStatus != new StringResourceModel("all", this, null).getString()) {
			qo.setStatus(statusRepoInstance.getByName(selectedStatus));
		}
		if (selectedCategory != new StringResourceModel("all", this, null).getString()) {
			qo.setCategory(categoryRepoInstance.getByName(selectedCategory));
		}
		if (selectedOrderField.equals(new StringResourceModel("name", this, null).getString())) {
			qo.setOrder(true);
			qo.setOrderByName(true);
		} else if (selectedOrderField.equals(new StringResourceModel("price", this, null).getString())) {
			qo.setOrder(true);
			qo.setOrderByPrice(true);
		} else if (selectedOrderField.equals(new StringResourceModel("date", this, null).getString())) {
			qo.setOrder(true);
			qo.setOrderByPublicationDate(true);
		}
		if (selectedOrderOrder.equals(new StringResourceModel("ascending", this, null).getString())) {
			qo.setDescending(false);
		} else {
			qo.setDescending(true);
		}
		return qo;
	}
	
	private void setSearchValuesInForm(QueryObject qo) {
		if (qo == null) {
			return;
		}
		if (qo.getName() != null) {
			name = qo.getName();
		}
		if (qo.getMinPrice() != -1) {
			from = qo.getMinPrice();
		}
		if (qo.getMaxPrice() != -1) {
			to = qo.getMaxPrice();
		}
		if (qo.getStatus() != null && qo.getStatus().getName() != new StringResourceModel("all", this, null).getString()) {
			selectedStatus = qo.getStatus().getName();
		}
		if (qo.getCategory() != null && qo.getCategory().getName() != new StringResourceModel("all", this, null).getString()) {
			selectedCategory = qo.getCategory().getName();
		}
		if (qo.isOrderByName()) {
			selectedOrderField = new StringResourceModel("name", this, null).getString();
		} else if (qo.isOrderByPrice()) {
			selectedOrderField = new StringResourceModel("price", this, null).getString();
		} else if (qo.isOrderByPublicationDate()) {
			selectedOrderField = new StringResourceModel("date", this, null).getString();
		}
		if (qo.isDescending()) {
			selectedOrderOrder = new StringResourceModel("descending", this, null).getString();
		} else {
			selectedOrderOrder = new StringResourceModel("ascending", this, null).getString();
		}
	}
}
