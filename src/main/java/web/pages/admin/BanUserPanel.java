package web.pages.admin;

import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.PawShopSession;

@SuppressWarnings("serial")
public class BanUserPanel extends Panel {
	@SpringBean
	private UserRepo userRepoInstance;

	public BanUserPanel(String id, IModel<UserClient> model) {
		super(id, model);
		add(new Label("username", model.getObject().getUsername()));
		Link<UserClient> banLink = new Link<UserClient>("ban", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.ban(getModelObject());
			}
			
			@Override
			protected void onBeforeRender() {
				setVisible(!getModelObject().getBanned());
				super.onBeforeRender();
			}
		};
		add(banLink);
		Link<UserClient> unbanLink = new Link<UserClient>("unban", model){
			@Override
			public void onClick() {
				PawShopSession s = PawShopSession.get();
				UserClient user = userRepoInstance.getByUsername(s.getUsername());
				user.unban(getModelObject());
			}
			
			@Override
			protected void onBeforeRender() {
				setVisible(getModelObject().getBanned());
				super.onBeforeRender();
			}
		};
		add(unbanLink);
	}
}
