package web.pages.admin;

import java.util.Date;

import org.apache.wicket.markup.html.link.Link;

import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class AdminProfilePage extends AdminPage {
	
	public AdminProfilePage() {
		add(new Link<Void>("usersLastAccess") {
			@Override
			public void onClick() {
				setResponsePage(UsersLastAccessPage.class);
			}
		});
		add(new Link<Void>("grantVipAccess") {
			@Override
			public void onClick() {
				setResponsePage(GrantVipAccessPage.class);
			}
		});
		add(new Link<Void>("banUsers") {
			@Override
			public void onClick() {
				setResponsePage(BanUsersPage.class);
			}
		});
		add(new Link<Void>("purchasesByCategory") {
			@Override
			public void onClick() {
				setResponsePage(PurchasesByCategoryPage.class);
			}
		});
		add(new Link<Void>("usersReputation") {
			@Override
			public void onClick() {
				setResponsePage(new UsersReputationPage(2));
			}
		});
		add(new Link<Void>("purchasesByDate") {
			@Override
			public void onClick() {
				setResponsePage(new PurchasesByDayPage(new Date(), new Date()));
			}
		});
	}
	
}
