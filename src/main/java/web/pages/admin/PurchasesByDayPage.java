package web.pages.admin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import models.CategoryRepo;
import models.EntityResolver;
import models.PurchaseRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.DatePanel;
import web.pages.secured.AdminPage;

@SuppressWarnings("serial")
public class PurchasesByDayPage extends AdminPage {
	
	@SpringBean
	private CategoryRepo categoryRepoInstance;
	
	@SpringBean
	private PurchaseRepo purchaseRepoInstance;

	@SpringBean
	private EntityResolver entityResolver;
	
	private transient int fromYear = Calendar.getInstance().get(Calendar.YEAR);
	private transient int fromMonth = 1;
	private transient int fromDay = 1;
	private transient int toYear =  Calendar.getInstance().get(Calendar.YEAR);
	private transient int toMonth = 1;
	private transient int toDay = 1;
	
	@SuppressWarnings("deprecation")
	public PurchasesByDayPage(Date from, Date to) {
		initializeFormValues(from, to);
		add(new FeedbackPanel("feedback"));
		Form<PurchasesByDayPage> form = new Form<PurchasesByDayPage>(
				"dateForm", new CompoundPropertyModel<PurchasesByDayPage>(this)) {
			@Override
			protected void onSubmit() {
				Date from = new Date(fromYear - 1900, fromMonth - 1, fromDay);
				Date to = new Date(toYear - 1900, toMonth - 1, toDay);
				if (from != null && to !=null && from.before(to)){
					setResponsePage(new PurchasesByDayPage(from, to));
				}
			}
		};

		List<Integer> days = new ArrayList<Integer>();
		List<Integer> months = new ArrayList<Integer>();
		List<Integer> year = new ArrayList<Integer>();

		for (int i = 1; i < 32; i++) {
			days.add(i);
		}
		for (int i = 1; i < 13; i++) {
			months.add(i);
		}
		year.add(Calendar.getInstance().get(Calendar.YEAR));
		year.add(Calendar.getInstance().get(Calendar.YEAR) - 1);

		DropDownChoice<Integer> fromYearDropDown = new DropDownChoice<Integer>(
				"fromYear", year);
		form.add(fromYearDropDown.setRequired(true));
		DropDownChoice<Integer> fromMonthDropDown = new DropDownChoice<Integer>(
				"fromMonth", months);
		form.add(fromMonthDropDown.setRequired(true));
		DropDownChoice<Integer> fromDayDropDown = new DropDownChoice<Integer>(
				"fromDay", days);
		form.add(fromDayDropDown.setRequired(true));

		DropDownChoice<Integer> toYearDropDown = new DropDownChoice<Integer>(
				"toYear", year);
		form.add(toYearDropDown.setRequired(true));
		DropDownChoice<Integer> toMonthDropDown = new DropDownChoice<Integer>(
				"toMonth", months);
		form.add(toMonthDropDown.setRequired(true));
		DropDownChoice<Integer> toDayDropDown = new DropDownChoice<Integer>(
				"toDay", days);
		form.add(toDayDropDown.setRequired(true));
		form.add(new Button("submit", new ResourceModel("submit")));
		add(form);
		add(new ListView<Integer>("row", purchaseRepoInstance.getTotalAmount(new Date(fromYear - 1900, fromMonth - 1, fromDay), new Date(toYear - 1900, toMonth - 1, toDay))) {

			private Date aux = new Date(fromYear - 1900, fromMonth - 1, fromDay);
			
			@Override
			protected void populateItem(ListItem<Integer> item) {
				item.add(new Label("date", aux.getDate()+"/"+(aux.getMonth()+1)+"/"+(aux.getYear()+1900)));
				item.add(new DatePanel("prettyDate", new Model<Date>(aux)));
				item.add(new Label("purchases", item.getModelObject().toString()));
				aux = new Date(aux.getTime()+TimeUnit.DAYS.toMillis(1));
			}
		});
		
	}

	@SuppressWarnings("deprecation")
	private void initializeFormValues(Date from, Date to) {
		if (from != null) {
			fromYear = from.getYear() + 1900;
			fromMonth = from.getMonth() + 1;
			fromDay = from.getDate();	
		}
		if (to != null) {
			toYear = to.getYear() + 1900;
			toMonth = to.getMonth() + 1;
			toDay = to.getDate();
		}
	}

}
