package web.pages.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import models.Article;
import models.ArticleRepo;
import models.EntityResolver;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.base.ArticleLinkPanel;
import web.pages.base.DatePanel;

@SuppressWarnings("serial")
public class RecentlyPublishedPanel extends Panel {
	
	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;

	public RecentlyPublishedPanel(String id) {
		super(id);
		initialize();
	}

	private void initialize() {
		add(new RefreshingView<Article>("recentlyPublished") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<Article>> getItemModels() {
				List<IModel<Article>> result = new ArrayList<IModel<Article>>();
				for (Article a : articleRepoInstance.getLastPublished(5)) {
					final int id = a.getId(); 
					result.add(new LoadableDetachableModel<Article>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected Article load() {
							return entityResolver.fetch(Article.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<Article> item) {
				item.add(new ArticleLinkPanel("articleLink", item.getModel()));
				item.add(new DatePanel("publicationDate", new PropertyModel<Date>(item.getModel(), "publicationDate")));
			}
		});
	}
}
