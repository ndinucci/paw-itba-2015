package web.pages.home;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.ArticleRepo;
import models.EntityResolver;
import models.UserClient;
import models.UserRepo;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RefreshingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import web.pages.userPublicProfile.UserPublicProfilePage;

@SuppressWarnings("serial")
public class SellersPanel extends Panel {
	
	@SpringBean
	private ArticleRepo articleRepoInstance;
	
	@SpringBean
	private EntityResolver entityResolver;

	@SpringBean
	private UserRepo userRepoInstance;
	
	public SellersPanel(String id) {
		super(id);
		initialize();
	}

	private void initialize() {
		add(new RefreshingView<UserClient>("sellers") {
			private static final long serialVersionUID = 1L;

			@Override
			protected Iterator<IModel<UserClient>> getItemModels() {
				List<IModel<UserClient>> result = new ArrayList<IModel<UserClient>>();
				for (UserClient u : userRepoInstance.getSellers()) {
					final int id = u.getId(); 
					result.add(new LoadableDetachableModel<UserClient>() {
						private static final long serialVersionUID = 1L;

						@Override
						protected UserClient load() {
							return entityResolver.fetch(UserClient.class, id);
						}
					});
				}
				return result.iterator();
			}

			@Override
			protected void populateItem(Item<UserClient> item) {
				Link<UserPublicProfilePage> link = new BookmarkablePageLink<UserPublicProfilePage>("usernameLink", UserPublicProfilePage.class, new PageParameters().add("username", item.getModelObject().getUsername())); 
				link.add(new Label("username", new PropertyModel<String>(item.getModel(), "username")));
				item.add(link);
			}
		});
	}
}
