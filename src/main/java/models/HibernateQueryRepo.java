package models;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateQueryRepo extends AbstractHibernateRepo implements
		QueryRepo {

	@Autowired
	public HibernateQueryRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Set<Query> getPendingQueriesFrom(UserClient user) {
		Set<Article> articles = user.getOwnedArticles();
		Set<Query> queries = new HashSet<Query>();
		for(Article article:articles){
			Set<Query> aux= article.getUnansweredQueries();
			queries.addAll(aux);
		}
		return queries;
	}

	public void ask(Query query) {
		save(query);
	}

	public Query getById(int id) {
		return get(Query.class, id);
	}

	public void delete(Query query) {
		super.delete(query);
	}
}
