package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateCategoryRepo extends AbstractHibernateRepo implements
		CategoryRepo {
	
	@Autowired
	public HibernateCategoryRepo(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Set<Category> getAll() {
		List<Category> list = find("from Category");
		Set<Category> categories = new HashSet<Category>();
		categories.addAll(list);
		return categories;
	}

	public Category getByName(String name) {
		List<Category> list=find("from Category where name=?",name);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}
	
}
