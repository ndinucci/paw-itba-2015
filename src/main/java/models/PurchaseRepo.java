package models;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface PurchaseRepo {
	
	public Set<Purchase> getAll();
	
	public Set<Purchase> getByBuyer(UserClient user);
	
	public Set<Purchase> getBySaler(UserClient user);
	
	public Set<Purchase> getByArticle(Article article);
	
	public void purchase(Purchase purchase);
	
	public Purchase getById(int id);
	
	public List<Integer> getTotalAmount(Date from, Date to);
}
