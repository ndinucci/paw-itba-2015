package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

@Repository
public class HibernateUserRepo extends AbstractHibernateRepo implements
		UserRepo {

	private PurchaseRepo purchaseRepoInstance;

	@Autowired
	public HibernateUserRepo(SessionFactory sessionFactory,
			PurchaseRepo purchaseRepoInstance) {
		super(sessionFactory);
		this.purchaseRepoInstance = purchaseRepoInstance;
	}
	
	public void add(UserClient user) {
		if (getByUsername(user.getUsername())!=null) {
			throw new DuplicateKeyException("usuario duplicado");
		}
		save(user);
	}

	public UserClient getByUsername(String username) {
		if(username==null){
			return null;
		}
		List<UserClient> users = find("from UserClient where username=?",
				username);
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	public UserClient getByEmail(String email) {
		List<UserClient> users = find("from UserClient where email=?", email);
		if (users.size() > 0) {
			return users.get(0);
		}
		return null;
	}

	public UserClient signIn(String username, String password) {
		UserClient u = null;
		if (username != null && password != null) {
			u = getByUsername(username);
			if (u != null) {
				if (!u.getPassword().equals(password)) {
					u = null;
				}
			}
		}
		return u;
	}

	public UserClient signUp(UserClient user) {
		if (user != null && user.getId() == -1) {
			UserClient bdUser = getByUsername(user.getUsername());
			UserClient bdEmail = getByEmail(user.getEmail());
			if (bdUser != null || bdEmail != null) {
				return null;
			}
		}
		add(user);
		return user;
	}

	public Set<UserClient> getSellers() {
		List<Article> articles = find("from Article");
		Set<UserClient> users = new HashSet<UserClient>();
		for(Article article:articles){
			users.add(article.getOwner());
		}
		return users;
	}

	public Set<UserClient> getBuyersOf(Article article) {
		Set<Purchase> purchases = purchaseRepoInstance.getByArticle(article);
		Set<UserClient> users = new HashSet<UserClient>();
		for (Purchase purchase : purchases) {
			users.add(purchase.getBuyer());
		}
		return users;
	}

	public void save(UserClient user) {
		super.save(user);
	}

	public Set<UserClient> getAll() {
		List<UserClient> list = find("from UserClient");
		Set<UserClient> users = new HashSet<UserClient>();
		users.addAll(list);
		return users;
	}
	
}
