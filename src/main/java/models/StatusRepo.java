package models;

import java.util.Set;

public interface StatusRepo {
	public Set<Status> getAll();

	public Status getByName(String name);
}
