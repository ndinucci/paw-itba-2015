package models;

import java.util.List;
import java.util.Set;

public interface ArticleRepo {
	public Article getById(int id);
	
	public List<Article> search(QueryObject queryObject);
	
	public List<Article> getMostVisited(int n);
	
	public List<Article> getLastPublished(int n);
	
	public Set<Article> getMostPurchased(int n);
	
	public Set<Article> getSuggestions(int n, Article article);
	
	public void publish(Article article);

}
