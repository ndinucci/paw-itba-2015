package models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.Entity;

@Entity
public class Category extends PersistentEntity {

	@Column(nullable = false)
	private String name;

	@ManyToMany(mappedBy = "categories")
	private Set<Article> articles = new HashSet<Article>();

	Category() {
		// For Hibernate
	}

	public Category(String name) {
		setId(-1);
		setProperties(name);
	}

	public Set<Article> getArticles() {
		return articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}

	private void setProperties(String name) {
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name.length() > 64) {
			throw new IllegalArgumentException("Category name is too long.");
		}
		this.name = name;
	}

	@Override
	public int hashCode() {
		return 31 + name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((Category) obj).getName().equals(this.getName())) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	public Integer getPurchases(){
		Integer purchases = 0;
		for(Article a : articles){
			purchases += a.getPurchases();
		}
		return purchases;
	}
	
}
