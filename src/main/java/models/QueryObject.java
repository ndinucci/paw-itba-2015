package models;

import models.Category;
import models.Status;
import models.UserClient;

public class QueryObject {

	private Category category;
	private Status status;
	private String name;
	private UserClient user;
	private float minPrice;
	private float maxPrice;
	private boolean order;
	private boolean descending;
	private boolean orderByName;
	private boolean orderByPrice;
	private boolean orderByPublicationDate;
	private boolean orderByVisits;
	
	public QueryObject() {
		this.category = null;
		this.status = null;
		this.name = null;
		this.user = null;
		this.minPrice = -1;
		this.maxPrice = -1;
		this.order = false;
		this.descending = false;
		this.orderByName = false;
		this.orderByPrice = false;
		this.orderByPublicationDate = false;
		this.orderByVisits = false;
	}

	public Category getCategory() {
		return category;
	}

	public Status getStatus() {
		return status;
	}

	public String getName() {
		return name;
	}

	public float getMinPrice() {
		return minPrice;
	}

	public float getMaxPrice() {
		return maxPrice;
	}

	public boolean isDescending() {
		return descending;
	}

	public boolean isOrderByName() {
		return orderByName;
	}

	public boolean isOrderByPrice() {
		return orderByPrice;
	}

	public boolean isOrderByPublicationDate() {
		return orderByPublicationDate;
	}

	public boolean isOrder() {
		return order;
	}

	public boolean isOrderByVisits() {
		return orderByVisits;
	}

	public UserClient getUser() {
		return user;
	}

	public void setUser(UserClient user) {
		this.user = user;
	}

	public void setOrderByVisits(boolean orderByVisits) {
		this.orderByVisits = orderByVisits;
	}

	public void setOrder(boolean order) {
		this.order = order;
	}

	public void setCategory(Category category) {
		if (category != null) {
			this.category = category;
		}
	}

	public void setStatus(Status status) {
		if (status != null) {
			this.status = status;
		}
	}

	public void setName(String name) {
		if (name != null) {
			this.name = name;
		}
	}

	public void setMinPrice(float minPrice) {
		if (minPrice >= 0) {
			this.minPrice = minPrice;
		}
	}

	public void setMaxPrice(float maxPrice) {
		if (maxPrice > 0) {
			this.maxPrice = maxPrice;
		}
	}

	public void setDescending(boolean descending) {
		this.descending = descending;
	}

	public void setOrderByName(boolean orderByName) {
		this.orderByName = orderByName;
	}

	public void setOrderByPrice(boolean orderByPrice) {
		this.orderByPrice = orderByPrice;
	}

	public void setOrderByPublicationDate(boolean orderByPublicationDate) {
		this.orderByPublicationDate = orderByPublicationDate;
	}
	
}
