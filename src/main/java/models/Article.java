package models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.postgresql.util.Base64;

@Entity
public class Article extends models.PersistentEntity {

	public final static int NAME_MAX_SIZE = 255;
	public final static float MIN_PRICE = 0f;
	public static final int DESCRIPTION_MAX_SIZE = 255;

	@Column(nullable = false)
	private int stock;

	@Column(nullable = false)
	private int visits;

	@Column(nullable = false)
	private int purchases;

	@Column(nullable = false)
	private float price;

	@Column(nullable = false)
	private String name;
	private String description;
	private Boolean isPrioritized;
	private Boolean suspended;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date publicationDate;

	@ManyToMany
	private Set<Category> categories = new HashSet<Category>();

	@OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
	private Set<Query> queries = new HashSet<Query>();

	@ManyToOne
	private Status status;

	@ManyToOne
	private UserClient owner;

	@Embedded
	private ArticleImage image;

	Article() {
		// For Hibernate
	}

	public Article(Integer stock, Float price, String name,
			String description, Date date, Set<Category> cats, Status byName,
			UserClient user, Boolean isPrioritized, byte[] imgBytes,
			String imgName, String imgType) {
		setProperties(stock, visits, price, purchases, name, description, date,
				cats, byName, user, isPrioritized, imgBytes, imgName, imgType);
	}

	private void setProperties(int stock, int visits, float price,
			int purchases, String name, String description,
			Date publicationDate, Set<Category> category, Status status,
			UserClient owner, Boolean isPrioritized, byte[] imgBytes,
			String imgName, String imgType) {
		setStock(stock);
		setVisits(visits);
		setPrice(price);
		setName(name);
		setDescription(description);
		setPublicationDate(publicationDate);
		addCategories(category);
		setStatus(status);
		setOwner(owner);
		setIsPrioritized(isPrioritized);
		if(imgBytes!=null){
			this.image = new ArticleImage(imgBytes, imgName, imgType);
		}
		suspended = false;
	}

	public byte[] getImageBytes() throws IOException {
		if (image == null) {
			return null;
		}
		return image.getBytes();
	}

	public String getImageBytesString() throws IOException {
		if (getImageBytes() == null) {
			return null;
		}
		return Base64.encodeBytes(getImageBytes());
	}

	public String getImageType() {
		if (image == null) {
			return null;
		}
		return image.getContentType();
	}

	public String getImageName() {
		if (image == null) {
			return null;
		}
		return image.getName();
	}

	private void setIsPrioritized(Boolean isPrioritized) {
		this.isPrioritized = isPrioritized;

	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		if (stock < 0) {
			throw new IllegalArgumentException("Invalid stock amount.");
		}
		this.stock = stock;
	}

	public Boolean getSuspended() {
		return suspended;
	}

	public int getVisits() {
		return visits;
	}

	private void setVisits(int visits) {
		this.visits = visits;
	}

	public float getPrice() {
		return price;
	}

	public int getPurchases() {
		return purchases;
	}

	public Set<Query> getQueries() {
		return queries;
	}

	public Set<Query> getUnansweredQueries() {
		Set<Query> ans = new HashSet<Query>();
		for (Query query : queries) {
			if (query.getAnswer() == null) {
				ans.add(query);
			}
		}
		return ans;
	}

	public void setPrice(float price) {
		if (price < 0) {
			throw new IllegalArgumentException("Invalid price.");
		}
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Invalid name.");
		} else if (name.length() > 32) {
			throw new IllegalArgumentException("Article name is too long.");
		}
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (description == null) {
			throw new IllegalArgumentException("Invalid description.");
		} else if (description.length() > 256) {
			throw new IllegalArgumentException("Description is too long.");
		}
		this.description = description;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	private void setPublicationDate(Date publicationDate) {
		if (publicationDate == null) {
			throw new IllegalArgumentException("Invalid date.");
		}
		this.publicationDate = publicationDate;
	}

	public Set<Category> getCategory() {
		return categories;
	}

	public void addCategories(Set<Category> categories) {
		if (categories == null || categories.isEmpty()) {
			throw new IllegalArgumentException("Invalid category.");
		}

		this.categories.addAll(categories);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		if (status == null) {
			throw new IllegalArgumentException("Invalid status.");
		}
		this.status = status;
	}

	public UserClient getOwner() {
		return owner;
	}

	private void setOwner(UserClient owner) {
		if (owner == null) {
			throw new IllegalArgumentException("Invalid owner.");
		}
		this.owner = owner;
	}

	public void visit() {
		visits++;
	}

	public Set<Article> getSameOwner(int n) {
		Set<Article> articles = new HashSet<Article>(getOwner()
				.getOwnedArticles());
		articles.remove(this);
		if (articles.size() > n) {
			List<Article> aux = new ArrayList<Article>();
			for (Article article : articles) {
				if (!article.equals(this)) {
					aux.add(article);
				}
			}
			Collections.shuffle(aux);
			articles = new HashSet<Article>();
			for (int i = 0; i < n; i++) {
				articles.add(aux.get(i));
			}
		}
		return articles;
	}

	public boolean sell(int amount) {
		if (amount > stock) {
			return false;
		}
		stock -= amount;
		return true;
	}

	public void cancelPurchase(Purchase purchase) {
		stock += purchase.getAmount();
	}

	public boolean isPrioritized() {
		return isPrioritized;
	}

	public void setPrioritized(boolean isPrioritized) {
		this.isPrioritized = isPrioritized;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public void edit() {
		// TODO CON FORMULARIO
	}

	@Override
	public int hashCode() {
		return 31 + name.hashCode() + owner.hashCode()
				+ publicationDate.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((Article) obj).getName().equals(this.getName())
					&& this.getOwner().equals(((Article) obj).getOwner())
					&& this.getPrice() == ((Article) obj).getPrice()
					&& this.getStock() == ((Article) obj).getStock()) {
				return true;
			}
			return false;
		}
		return false;
	}

	public void confirm(int amount) {
		purchases += amount;
	}

	public void ask(Query query) {
		queries.add(query);
	}

	public void deleteQuery(Query query) {
		queries.remove(query);
	}

	public void suspend() {
		suspended = true;
	}

	public void setImage(byte[] imgBytes, String imgName, String imgType) {
		if(imgBytes!=null && imgName!=null && imgType!=null){
			this.image = new ArticleImage(imgBytes, imgName, imgType);
		}
	}

	public void unsuspend() {
		suspended = false;		
	}
}
