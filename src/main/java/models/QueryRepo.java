package models;

import java.util.Set;

public interface QueryRepo {
	public Set<Query> getPendingQueriesFrom(UserClient user);
	public void ask(Query query);
	public Query getById(int id);
	public void delete(Query query);
}
