package models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Purchase extends PersistentEntity {

	public static final int COMMENT_MAX_SIZE = 255;
	@Column(nullable = false)
	private int amount;
	private String comment;
	private boolean isConfirmed;
	private boolean isReported;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date purchaseDate;

	@ManyToOne
	private UserClient buyer;

	@ManyToOne(cascade=CascadeType.ALL)
	private Article article;

	Purchase() {
		// For Hibernate
	}

	public Purchase(int amount, String comment, Date purchaseDate,
			UserClient buyer, Article article) {
		setProperties(amount, comment, purchaseDate, buyer, article);
	}

	private void setProperties(int amount, String comment, Date purchaseDate,
			UserClient buyer, Article article) {
		setAmount(amount);
		setComment(comment);
		setPurchaseDate(purchaseDate);
		setBuyer(buyer);
		setArticle(article);
		isConfirmed = false;
		isReported = false;
	}

	public void confirm() {
		if (!isReported){
			isConfirmed = true;
		}
		article.confirm(amount);
	}

	public void report() {
		if (!isConfirmed) {
			isReported = true;
		}
		article.cancelPurchase(this);
	}

	public int getAmount() {
		return amount;
	}
	
	private void setAmount(int amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException("Amount must be greater than 0");
		}
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	private void setComment(String comment) {
		if (comment.length() > 256) {
			throw new IllegalArgumentException(
					"Comments must have no more than 256 characters");
		}
		this.comment = comment;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	private void setPurchaseDate(Date operationDate) {
		if (operationDate == null) {
			throw new IllegalArgumentException("operation date is null");
		}
		this.purchaseDate = operationDate;
	}

	public UserClient getBuyer() {
		return buyer;
	}

	private void setBuyer(UserClient buyer) {
		if (buyer == null) {
			throw new IllegalArgumentException("buyer is null");
		}
		this.buyer = buyer;
	}

	public Article getArticle() {
		return article;
	}

	private void setArticle(Article article) {
		if (article == null) {
			throw new IllegalArgumentException("article is null");
		}
		this.article = article;
	}

	public boolean isConfirmed() {
		return isConfirmed;
	}
	
	public boolean getisConfirmed(){
		return isConfirmed();
	}
	
	public boolean getisReported(){
		return isReported;
	}

	@Override
	public int hashCode() {
		return 31 + article.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			if (((Purchase) obj).getBuyer().equals(this.getBuyer())
					&& ((Purchase) obj).getArticle().equals(this.getArticle())
					&& ((Purchase) obj).getPurchaseDate().equals(
							this.getPurchaseDate())) {
				return true;
			}
			return false;
		}
		return false;
	}

}
